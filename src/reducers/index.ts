import {AnyAction} from "redux";
import {SET_TV_SHOW} from "../actions";
import {State} from "../store/configure-store";

const initialState: State = {
    tvShow: {
        id: '',
        description: '',
        coverImage: {
            medium: '',
            original: ''
        },
        episodes: [],
        title: ''
    }
};
export const reducer = (state: State = initialState, action: AnyAction) => {
    switch (action.type) {
        case SET_TV_SHOW:
            return {
                ...state,
                tvShow: action.payload
            };
        default:
            return state;
    }
};
