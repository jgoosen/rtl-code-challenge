import React, {useEffect} from "react";
import {useParams} from "react-router-dom"

import {TvShow} from "../../domain/tv-show";
import ShowDetail from "./show-detail";
import {useDispatch, useSelector} from "react-redux";

import {thunkedSetTvShowById} from "../../actions";
import {State} from "../../store/configure-store";


const ShowDetailContainer: React.FC = () => {

    const {id} = useParams<{ id: string }>();

    const dispatch = useDispatch();

    const showDetails = useSelector<State, TvShow>(state => state.tvShow);

    useEffect(() => {
        dispatch(thunkedSetTvShowById(id));
    }, [id, dispatch]);

    return (
        <React.Fragment>
            {showDetails && <ShowDetail showDetails={showDetails} />}
        </React.Fragment>
    );

};

export default ShowDetailContainer;
