import React, {memo} from "react";
import {TvShow} from "../../domain/tv-show";
import Poster from "../poster/poster";
import LinkList from "../link-list/link-list";
import {episodeToLink} from "../../api/helpers";
import "./show-detail.scss"

interface ShowDetailProps {
    showDetails: TvShow
}

const ShowDetail: React.FC<ShowDetailProps> = ({showDetails}) => {

    return (
        <div className="detail">
            <div className="poster-container">
                <Poster coverImage={showDetails.coverImage} title={showDetails.title}/>
            </div>

            <div className="container">
                <h1>{showDetails.title}</h1>

                <div className="show-description" dangerouslySetInnerHTML={{__html: showDetails.description}}></div>

                <h2>Available episodes</h2>

                <LinkList links={showDetails.episodes.map(episodeToLink)} />


            </div>
        </div>
    );

};

export default memo<ShowDetailProps>(ShowDetail);

