import React, {memo} from "react";
import {CoverImage} from "../../domain/tv-show";
import "./poster.scss";

export interface PosterProps {
    coverImage: CoverImage,
    title: string
}

export const Poster: React.FC<PosterProps> = ({coverImage, title}) => {

    return (
        <div className="poster">
            <img src={coverImage.original} alt={title} title={title} className="poster__img" />
        </div>

    );
    
};

export default memo<PosterProps>(Poster);
