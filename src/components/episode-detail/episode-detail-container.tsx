import React, {useEffect} from "react";
import EpisodeDetail from "./episode-detail";
import {useDispatch, useSelector} from "react-redux";
import {Episode} from "../../domain/tv-show";
import {useParams} from "react-router";
import {State} from "../../store/configure-store";
import {thunkedSetTvShowByEpisodeId} from "../../actions";

const EpisodeDetailContainer: React.FC = () => {

    const {id} = useParams<{ id: string }>();

    const episode = useSelector<State, Episode | undefined>(state => {
        return state.tvShow.episodes.find((episode) => {
            return episode.id === id;
        });
    });

    const dispatch = useDispatch();

    const showTitle = useSelector<State, string>(state => {
        return state.tvShow.title;
    });

    const showId = useSelector<State, string>(state => {
        return state.tvShow.id;
    });

    const linkToShow = `/shows/${encodeURIComponent(showId)}`;

    useEffect(() => {
        dispatch(thunkedSetTvShowByEpisodeId(id));
    }, [id, dispatch]);

    return (
        <React.Fragment>
            {episode && <EpisodeDetail episode={episode} showTitle={showTitle} linkToShow={linkToShow}/>}
        </React.Fragment>
    );

};

export default EpisodeDetailContainer;


