import React, {memo} from "react";
import {Episode} from "../../domain/tv-show";
import Poster from "../poster/poster";
import {Link} from "react-router-dom";

interface EpisodeDetailProps {
    episode: Episode,
    showTitle: string,
    linkToShow: string
}

const EpisodeDetail: React.FC<EpisodeDetailProps> = ({episode, showTitle, linkToShow}) => {

    return (

        <div className="detail">

            <div className="poster-container">
                {episode.coverImage && <Poster coverImage={episode.coverImage} title={episode.title} />}
            </div>

            <div className="container">
                <span>Episode of <Link to={linkToShow}>{showTitle}</Link></span>
                <h1>{episode.title}</h1>

                <div className="show-description" dangerouslySetInnerHTML={{__html: episode.summary}}></div>

            </div>

        </div>

    );

};

export default memo<EpisodeDetailProps>(EpisodeDetail);




