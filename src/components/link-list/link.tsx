import React from "react";
import {Url} from "../../domain/tv-show";
import { Link } from 'react-router-dom';

export interface IAppLink {
    url: Url,
    title: string,
    id: string
}

interface LinkProps {
    link: IAppLink
}

const AppLink: React.FC<LinkProps> = ({link}) => {
    return (
        <li  className="link-list__li"><Link to={link.url} className="link-list__link">{link.title}</Link></li>

    );
};

export default AppLink;
