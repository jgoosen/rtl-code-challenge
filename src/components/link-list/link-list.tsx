import React from "react";
import AppLink, {IAppLink} from "./link";
import "./link-list.scss";

interface LinkListProps {
    links: IAppLink[]
}

const LinkList: React.FC<LinkListProps> = ({links}) => {

    return (

        <ul className="link-list">
            {links.map((link: IAppLink) => {
                return <AppLink key={link.id} link={link}/>
            })}
        </ul>

    );


};


export default LinkList;
