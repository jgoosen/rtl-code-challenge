import React from "react";

const RouteDoesNotExist: React.FC = () => {

    return (
        <p>Sorry, the page you have requested does not exist.</p>
    );

};

export default RouteDoesNotExist;


