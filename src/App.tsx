import React from 'react';
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";
import * as ReactRedux from 'react-redux';
import './App.scss';
import RouteDoesNotExist from "./components/route-does-not-exist/route-does-exist";
import ShowDetailContainer from "./components/show-detail/show-detail-container";
import EpisodeDetailContainer from "./components/episode-detail/episode-detail-container";
import {store} from "./store/configure-store";

const App: React.FC = () => {
    return (
        <div className="App">

            <header className="container">
                <span>Your TV guide</span>
            </header>

            <main>

                <BrowserRouter>
                    <ReactRedux.Provider store={store}>
                        <Switch>
                            <Redirect from="/" exact to="/shows/6771"/>
                            <Route exact path="/shows/:id" component={ShowDetailContainer}/>
                            <Route exact path="/episodes/:id" component={EpisodeDetailContainer}/>
                            <Route component={RouteDoesNotExist}/>
                        </Switch>
                    </ReactRedux.Provider>
                </BrowserRouter>

            </main>

            <footer className="container"></footer>

        </div>
    );
};

export default App;
