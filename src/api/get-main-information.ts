import {TvShow} from "../domain/tv-show";
import {BASE} from "./constants";

const PATH = 'shows';

const getMainInformation = (id: string): Promise<TvShow> => {
    return fetch(`${BASE}/${PATH}/${encodeURIComponent(id)}`)
        .then(response => response.json())
        .then(apiResonse => apiResponseToTvShow((apiResonse)));
};

const apiResponseToTvShow = (apiResponse: any): TvShow => {
    return {
        id: apiResponse.id.toString(),
        title: apiResponse.name,
        description: apiResponse.summary,
        coverImage: apiResponse.image,
        episodes: []
    } as TvShow;
};

export {getMainInformation};

