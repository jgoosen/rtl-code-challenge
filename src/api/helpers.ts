import {Episode, Url} from "../domain/tv-show";
import {IAppLink} from "../components/link-list/link";

export const getEpisodeLink = (id: string): Url => {
    const PATH = 'episodes';
    return `/${PATH}/${encodeURIComponent(id)}`;

};

export const episodeToLink = (episode: Episode): IAppLink => {
    return {
        title: episode.title,
        url: getEpisodeLink(episode.id),
        id: episode.id
    }
};

