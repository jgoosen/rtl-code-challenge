import {CoverImage, Episode} from "../domain/tv-show";
import {BASE} from "./constants";

const getEpisodes = (id: string): Promise<Episode[]> => {
    return fetch(`${BASE}/shows/${encodeURIComponent(id)}/episodes`)
        .then(response => response.json())
        .then(apiResonse => apiResponseToEpisodes((apiResonse)));
};

const apiResponseToEpisodes = (apiResponse: any[]): Episode[] => {

    return apiResponse.map((episode: any) => {
       return {
           id: episode.id.toString(),
           title: episode.name,
           summary: episode.summary,
           coverImage: episode.image as CoverImage
       }
    });

};

export {getEpisodes};
