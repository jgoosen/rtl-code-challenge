export type TvShow = {
    id: string,
    title: string,
    description: string,
    episodes: Episode[],
    coverImage: CoverImage
};

export type Episode = {
    id: string,
    title: string,
    summary: string,
    coverImage: CoverImage
};

export type CoverImage = {
    medium: Url,
    original: Url
};

export type Url = string;
