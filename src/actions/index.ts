import {AnyAction, Dispatch} from "redux";
import {getMainInformation} from "../api/get-main-information";
import {getEpisodes} from "../api/get-episodes";
import {Episode, TvShow} from "../domain/tv-show";
import {State} from "../store/configure-store";
import {BASE} from "../api/constants";

export const SET_TV_SHOW = 'SET_TV_SHOW';

export const setTvShow = (show: TvShow): AnyAction => {
    return {
        type: SET_TV_SHOW,
        payload: show
    };
};

export const thunkedSetTvShowById = (id: string) => {

    return (dispatch: Dispatch<any>, getState: any) => {

        const state: State = getState();

        if (state.tvShow.id === id) {
            return;
        }

        getTvShowById(id).then(([show, episodes]: [TvShow, Episode[]]) => {
            show.episodes = episodes;
            dispatch(setTvShow(show));
        }, (e) => {
            // TODO: implement error handling
        });

    };

};

export const thunkedSetTvShowByEpisodeId = (id: string) => {

    return (dispatch: Dispatch<any>, getState: any) => {

        const state: State = getState();

        if (state.tvShow.episodes.some(episode => episode.id === id)) {
            return;
        }

        getTvShowByEpisodeId(id).then(([show, episodes]: [TvShow, Episode[]]) => {
            show.episodes = episodes;
            dispatch(setTvShow(show));
        }, (e) => {
            // TODO: implement error handling
        });

    };

};

export const getTvShowById = (id: string): Promise<[TvShow, Episode[]]> => {
    return Promise.all([getMainInformation(id), getEpisodes(id)]);
};

const getTvShowByEpisodeId = (id: string) => {
    return fetch(`${BASE}/episodes/${encodeURIComponent(id)}?embed=show`)
        .then(response => response.json())
        .then(apiResonse => getTvShowById(apiResonse._embedded.show.id));
};

