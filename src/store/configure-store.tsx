import * as Redux from 'redux';
import {TvShow} from "../domain/tv-show";
import {Store} from "redux";
import {reducer} from "../reducers";
import {applyMiddleware} from "redux";
import thunk from "redux-thunk";

export interface State {
    tvShow: TvShow
}

export const store: Store<State> = Redux.createStore(
    reducer,
    (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(thunk)
);


